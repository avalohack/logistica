<?php
if (!defined('_PS_VERSION_')) exit;
class Logistica extends CarrierModule {

  private $_html = '';

  public $P_LOGIS_COD_CLIENTE;
  public $P_LOGIS_CLAVE;
  public $P_LOGIS_KEY;
  public $P_LOGIS_CIUDAD_ORIGEN; 

  function __construct() {
    parent::__construct(); //llamada al constructor padre.
    $this->name = 'logistica'; //nombre del módulo el mismo que la carpeta y la clase.
    $this->version = '1.0.0'; //versión del módulo
    $this->author ='APPSCOL'; // autor del módulo
    $this->need_instance = 1; //si no necesita cargar la clase en la página módulos,1 si fuese necesario.
    $this->tab="shipping_logistics";
    $this->ps_versions_compliancy = array('min' => '1.7.x.x', 'max' => _PS_VERSION_); //las versiones con las que el módulo es compatible.
    $this->bootstrap = true; //si usa bootstrap plantilla responsive.
 
    $this->displayName = $this->l('Logistica en Colombia'); // Nombre del módulo
    $this->description = $this->l('Cotiza tus envios en diferentes transportadoras de Colombia.'); //Descripción del módulo
    $this->confirmUninstall = $this->l('¿Estás seguro de que quieres desinstalar el módulo?'); //mensaje de alerta al desinstalar el módulo.

    $config = Configuration::getMultiple(array('P_LOGIS_COD_CLIENTE', 'P_LOGIS_CLAVE','P_LOGIS_KEY'));
    if (isset($config['P_LOGIS_COD_CLIENTE']))
        $this->P_LOGIS_COD_CLIENTE = trim($config['P_LOGIS_COD_CLIENTE']);
    if (isset($config['P_LOGIS_KEY']))
        $this->P_LOGIS_KEY= trim($config['P_LOGIS_KEY']);
    if (isset($config['P_LOGIS_CLAVE']))
        $this->P_LOGIS_CLAVE = $config['P_LOGIS_CLAVE'];
    if (isset($config['P_LOGIS_CIUDAD_ORIGEN']))
        $this->P_LOGIS_CIUDAD_ORIGEN = $config['P_LOGIS_CIUDAD_ORIGEN'];
 	

  }

    public function install()
    {
        
if (parent::install()) {
      // hooks
      $this->registerHook('extraCarrier');
       // fin hooks
       // parametro 
      if (!isset($this->P_LOGIS_COD_CLIENTE))
          Configuration::updateValue('P_LOGIS_COD_CLIENTE', '');
      if (!isset($this->P_LOGIS_CLAVE))
          Configuration::updateValue('P_LOGIS_CLAVE', '');
       if (!isset($this->P_LOGIS_KEY))
          Configuration::updateValue('P_LOGIS_KEY', '');
       if (!isset($this->P_LOGIS_CIUDAD_ORIGEN))
          Configuration::updateValue('P_LOGIS_CIUDAD_ORIGEN', '');
      return true;

      } else {
        return false;
      }


    }

     function uninstall() {
      if (!Configuration::deleteByName('P_LOGIS_COD_CLIENTE')
       OR !Configuration::deleteByName('P_LOGIS_CLAVE') 
       OR !Configuration::deleteByName('P_LOGIS_KEY') 
       OR !Configuration::deleteByName('P_LOGIS_CIUDAD_ORIGEN') 
       OR !parent::uninstall())
          return false;
      return true;
    }

  public function getContent(){
      $this->_html = '<h2>' . $this->displayName . '</h2>';

      if (Tools::isSubmit('btnSubmit')) {
        $this->_postValidation();
        if (!count($this->_postErrors)) {
          $this->_postProcess();
        } else {
          foreach ($this->_postErrors as $err) {
            $this->_html .= '<div class="alert error">' . $err . '</div>';
          }
        }
      } else {
        $this->_html .= '<br/>';
      }

      $x=$this->_displayForm();
      return $this->_html.$x;

  }

  private function _postValidation() 
  {
      if (Tools::isSubmit('btnSubmit')) 
      	{
	      /*  if (!Tools::getValue('PLOGISCODCLIENTE'))
	          $this->_postErrors[] = $this->l('El Campo Codigo es Requerido.');
	        if (!Tools::getValue('PLOGISCLAVE'))
	          $this->_postErrors[] = $this->l('El Campo Clave es Requerido.');
	      
	        if (!Tools::getValue('PLOGISKEY'))
	          $this->_postErrors[] = $this->l('El Campo LLave de acceso es Requerido.');
			*/
     	 }

    }

   private function _postProcess() {
      if (Tools::isSubmit('btnSubmit')) {
        Configuration::updateValue('P_LOGIS_COD_CLIENTE', Tools::getValue('PLOGISCODCLIENTE'));
        Configuration::updateValue('P_LOGIS_CLAVE', Tools::getValue('PLOGISCLAVE'));
        Configuration::updateValue('P_LOGIS_KEY', Tools::getValue('PLOGISKEY'));

        $this->_html.= '<div class="bootstrap"><div class="alert alert-success">'.$this->l('Datos actualizados de manera correcta') . '</div></div>';
      }
    }

    private function _displayForm() {
      // contruyendo el configurador
    $html='
    <form action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'" method="post" class="">

    <table border="0" width="90%" cellpadding="5" cellspacing="0" id="formlogistica" align="center">
    <tr><td colspan="2">Para acceder al servicio, debe ingresar los siguientes par&aacute;metros. (Contacte a su proveedor para solicitarlos):<br></td></tr>
    <tr><td>C&oacute;digo. '.utf8_encode("(Es el n&uacute;mero con el que esta asignado)").'</td><td>
<input type="text" id="PLOGISCODCLIENTE" name="PLOGISCODCLIENTE" value="' . Tools::htmlentitiesUTF8(Tools::getValue('PLOGISCODCLIENTE', $this->P_LOGIS_COD_CLIENTE)) . '" maxlength="9" size="30" required/>
    </td></tr>

    <tr><td>Clave. '.utf8_encode("(Es la clave que le asignaron)").'</td><td>
<input type="text" id="PLOGISCLAVE" name="PLOGISCLAVE" value="' . Tools::htmlentitiesUTF8(Tools::getValue('PLOGISCLAVE', $this->P_LOGIS_CLAVE)) . '" maxlength="100" size="30" required/>
    </td></tr>


    <tr><td>LLave de acceso.</td><td>
<input type="text" id="PLOGISKEY" name="PLOGISKEY" value="' . Tools::htmlentitiesUTF8(Tools::getValue('PLOGISKEY', $this->P_LOGIS_KEY)) . '" maxlength="100" size="30" required/>
    </td></tr>

 <tr><td>Ciudad origen.</td><td>
<input type="text" id="PLOGIS_CIUDAD" name="PLOGIS_CIUDAD" value="' . Tools::htmlentitiesUTF8(Tools::getValue('PLOGIS_CIUDAD', $this->P_LOGIS_CIUDAD_ORIGEN)) . '" maxlength="100" size="30" required/>
    </td></tr>


    <tr><td colspan="2">
    <button type="submit" name="btnSubmit" value="' . $this->l('Guardar la configuración') . '"/>Guardar la configuración</button>

    </td></tr>


    </table>
    </form>
    ';
    return $html;    
    }  

  public function hookDisplayHeader($params){
      //$this->context->controller->addCSS($this->_path.'css/logistica.css', 'all');
  }

function hookextraCarrier($params){

}

    public function getOrderShippingCost($params, $shipping_cost)
  {
  }

    public function getOrderShippingCostExternal($params)
  {
    return true;
  }

    

} // fin clase

?>
